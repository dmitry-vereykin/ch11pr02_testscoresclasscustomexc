/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
import java.util.Scanner;

public class TestScoresDemo {
    public static void main(String[] args) throws InvalidTestScore {
        int count;

        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter number of test scores:");
        count = userInput.nextInt();

        double[] scoreArray = new double[count];

        for (int i = 0; i < count; i++) {
            System.out.print("Enter test score #" + (i + 1) + ": ");
            scoreArray[i] = userInput.nextDouble();
        }

        new TestScores(scoreArray);

    }
}
