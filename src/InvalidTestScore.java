/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
public class InvalidTestScore extends Exception  {
    public InvalidTestScore(int i) {
        super("Score #"+ (i + 1) + " is out of range.");
    }
}